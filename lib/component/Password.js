import React,{ Component, PropTypes } from 'react';
import { View } from 'react-native';
import { Text, FormInput, FormLabel, Button } from 'react-native-elements';
import Controller from '../controller/userController';
import MessageText from './MessageText';

import routes from '../routes';
const controller = new Controller();
const propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
    userInfo : PropTypes.object.isRequired
};
const wrongPasswrd = ['1234','123456789','12341234','passwrod','PASSWORD','password1'];
class Password extends Component{
    constructor(props, context){
        super(props, context);
        this.state = {
            password : '',
            check_password : '',
            message : ''
        };
        //如果沒有給過來帳號資訊就退回
        if( !this.props.userInfo ) this.props.navigator.pop( routes.forget );
        
    }
    checkPasswrodFormat(){
        let { password, check_password } = this.state;
        let answer = true;
        let message ='';

        if( password.length < 6 )answer = false, message="密碼長度不可小於6位數";
        if( !(/^[A-Z]/).test(password) ) answer = false, message = '密碼開頭必須要為大寫英文字';
        if( wrongPasswrd.indexOf(password) > -1 ) answer = false, message = '不可以設定簡單密碼';
        if( !password || !check_password ) answer =false,  message = "請輸入密碼兩次";
        if( password !== check_password ) answer =false, message = "兩次輸入的密碼不相同";

        return { answer, message };
    }
    async _handelSubmitData(){
        let checkPassword = this.checkPasswrodFormat();

        if( !checkPassword.answer ){
            this.setState({ message : checkPassword.message });
        }else{
            let JSONbody = controller.setPostprivateKey({
                account : this.props.userInfo.MF001,
                _password : this.state.password
            });

            let res = await controller.postAPI(`api/user/reset-password`, JSONbody);

            this.setState({ message : res.message });
            if( res.answer ){
                setTimeout(()=>{
                    this.props.navigator.pop( routes.home );
                }, 1500);
            }

        }

    }
    render(){
        return(
            <View>
                <Text h3 style={{ textAlign : 'center', marginTop: 15 }}>設定密碼</Text>
                 <MessageText message={this.state.message} />
                <FormLabel>帳號：{this.props.userInfo.MF001}</FormLabel>
                <FormLabel>新密碼：</FormLabel>
                <FormInput 
                    secureTextEntry={true} 
                    onChangeText={(password)=> this.setState({ password : password }) }
                    ref={(el)=>{ this.password = el }}
                />
                <FormLabel>重複新密碼：</FormLabel>
                <FormInput 
                    secureTextEntry={true} 
                    onChangeText={(check_password)=> this.setState({ check_password : check_password }) }
                    ref={(el)=>{ this.check_password = el }}
                />
                <Text></Text>
                <Button title="送出" onPress={this._handelSubmitData.bind(this)} />
            </View>
        );
    }
}

Password.propTypes = propTypes
export default Password;