import React, { Component, PropTypes } from 'react';
import { View, StyleSheet, ScrollView, Modal } from 'react-native';
import { FormLabel, FormInput, Button, Text, ButtonGroup } from "react-native-elements";
import Calendar from 'react-native-calendar-picker';

import MessageText from './MessageText';
import ListBtn from './ListBtn';

import routes from '../routes';
import Header from './Header';
import Controller from '../controller/userController';

const styles = StyleSheet.create({
    content : {
        flex : 1,
        flexDirection : "column",
        alignItems : "center"
    }
});
class Home extends Component{
    constructor(props, context){
        super(props, context);

        this.state = {
            message : '預設抓取本月份工單',
            planList : {},
            BtnGroupSelected : 0,
            startDate : ``,//生產計畫搜尋起始時間
            endDate : ``,//生產計畫搜尋結束時間
            modelContnet : {}, //依照btn group 選定的項目顯示modal中的內容
            showModal : false
        };
        // global.storage.load({ key:"userInfo" }).then(user=>{
        //     console.log( user );
        // });
        //非正式環境
        if( global.config.isProduction ){
            global.storage.load({ key : "userInfo" }).then( data =>{
                if( !data ) this.props.navigator.pop( routes.login );
            });
        }
        this._handlePlantList();
        
    }
    componentDidMount(){
        this._handleTabChange();//一進來先開啟Modal
    }
    async _handlePlantList():void{

        let controller = new Controller();
        let JSONbody = controller.setPostprivateKey({
            start_date : this.state.startDate,
            end_date : this.state.endDate,
        });

        let plantList = await controller.postAPI(`api/plant/list`, JSONbody);
        if( plantList.answer ){
            this.setState({
                planList : plantList.data,
                message : plantList.message,
                showModal : false
            })
        }else{
            
            this.setState({
                planList : {},
                message : plantList.message
            })
        }
    }  
    async _handleChangeDate(data, type){
        
        switch (type) {
            case `START_DATE`:
                await this.setState({ startDate : data });
                break;
            case `END_DATE`:
                await this.setState({ endDate : data });
                break;

        }
        let { startDate, endDate } = this.state;
        
        if( startDate && endDate ){
            this._handlePlantList();
        }else{
            this.setState({ message : `請選擇日期區間` })
        }
    }
    async _handleOrderNo(){
        let controller = new Controller();
        let JSONbody = controller.setPostprivateKey({
            poNo : this.state.orderNo,
        });

        let plantList = await controller.postAPI(`api/plant/list-by-po`, JSONbody);
        if( plantList.answer ){
            this.setState({
                planList : plantList.data,
                message : plantList.message,
                showModal : false
            })
        }else{
            
            this.setState({
                planList : {},
                message : plantList.message
            })
        }
    }
    async _handleTabChange(BtnGroupSelected=''){

        ( BtnGroupSelected !== '' )?await this.setState({ BtnGroupSelected }) : "";
        switch (this.state.BtnGroupSelected) {
            case 0://日期區間查詢
                await this.setState({
                    modelContnet : 
                    <View>
                        <FormLabel>選擇日期區間</FormLabel>
                        <MessageText message={this.state.message} />
                        <Calendar
                            startFromMonday={true}
                            allowRangeSelection={true}
                            onDateChange={this._handleChangeDate.bind(this)}
                        />
                    </View>
                })
                break;
            case 1://依訂單查詢
                await this.setState({
                    modelContnet : 
                    <View>
                        <FormLabel>依照訂單編號查詢 ：</FormLabel>
                        <MessageText message={this.state.message} />
                        <FormInput 
                            onChangeText={ orderNo=>{ this.setState({ orderNo }) } }
                            ref={(el)=>{ this.orderNo = el }}
                        />
                        <Text></Text>
                        <Button backgroundColor='' icon={{ name : 'envira' , type: 'font-awesome' }} title='查詢' onPress={this._handleOrderNo.bind(this)}/>
                    </View>
                })
                break;
        }
        this.setState({ showModal : true })
    }
    render(){
        let dataRow = [];
        if( this.state.planList ){
            for( let v =0; v<this.state.planList.length; v++ ){
                dataRow.push(<ListBtn 
                        key={v} 
                        navigator={this.props.navigator} 
                        route={this.props.route} 
                        dataRow={this.state.planList[v]}
                    />
                );
            }
        }   
        
        return (
            <View style={styles.content}>
                <Header title="生產計畫列表"/>
                <MessageText message={this.state.message} />
                <ButtonGroup
                    onPress={ this._handleTabChange.bind(this) }
                    selectedIndex={this.state.BtnGroupSelected}
                    buttons={['日期區間查詢',"訂單編號查詢"]}
                    containerStyle={{height: 30}}
                />
                <View style={{  alignSelf: 'stretch' }}>
                    <ScrollView style={{ marginLeft : 5, marginRight : 5, marginTop : 5, height : 530}}>
                        {dataRow}
                    </ScrollView>
                </View> 
                <Modal
                    animationType={"fade"}
                    visible={this.state.showModal}
                    onRequestClose={ ()=> { return true }}
                >  
                    <View style={{ flexDirection: "row" , alignSelf : 'flex-end', marginTop : 15, marginRight : -10}}>
                        <Button title='關閉' 
                            onPress={ ()=>{
                                this.setState({ showModal : false });
                            }} 
                        textStyle={{ color : "blue" }} 
                        buttonStyle={{ backgroundColor : 'white' }} />
                    </View>
                    <View>
                        {this.state.modelContnet}
                    </View>
                    
                </Modal>
            </View>
        );
    }
}

Home.propTypes = {
    navigator: PropTypes.object.isRequired,
    route: PropTypes.object.isRequired,
}
export default Home;