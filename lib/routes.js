import Home from './component/Home';
import Login from './component/Login';
import Forget from './component/Forget';
import Password from './component/Password';

export default{
    login : {
        title : 'login component',
        Page : Login
    },
    forget : {
        title : 'forget password component',
        Page : Forget
    },
    password : {
        title : 'set password component',
        Page : Password
    },
    home : {
        title : 'home component',
        Page : Home
    },

}